#include <Adafruit_NeoPixel.h>
//setup LED pins
#define PIN0 9
#define PIN1 10
#define PIN2 11
#define PIN3 12

//setup RF pins
const int aPin = 37;
const int bPin = 35;
const int cPin = 33;
const int dPin = 31;

//setup rf variables
int aState = 0;
int bState = 0;
int cState = 0;
int dState = 0;

//setup counters for triggers
int first = 0;
int off = 0;

//setup LED values
int R = 0;  //lowest R value
int G = 0;  //lowest G value
int B = 0;  //lowest B value

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)

Adafruit_NeoPixel strip0 = Adafruit_NeoPixel(135, PIN0, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(135, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(135, PIN2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip3 = Adafruit_NeoPixel(135, PIN3, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void setup() {
   //serial begin for debugging 
  Serial.begin(9600);
  
  strip0.begin();
  strip1.begin();
  strip2.begin();
  strip3.begin();
 // Initialize all pixels to 'off'
  strip0.show();
  strip1.show();
  strip2.show();
  strip3.show();

//listen to rf prins setup
  pinMode(aPin, INPUT); 
  pinMode(bPin, INPUT); 
  pinMode(cPin, INPUT); 
  pinMode(dPin, INPUT); 
  
//confirm via serial that start is complete
  Serial.println("startup complete"); 
  

}

void loop()///MAIN LOOP
{
//Serial.println("read switch");
//read switch pin status
  aState = digitalRead(aPin);
  bState = digitalRead(bPin);
  cState = digitalRead(cPin);
  dState = digitalRead(dPin);

//setup LED fade vars

  int finCount=6;  //step in size (lower=slower and smoother)
  int foutCount=6;  //step out size
  int Rset = 245;  //highest R val
  int Gset = 245;  //highest G val
  int Bset = 245;  //highest B val
  int waitT = 25;//how long to wait at each stage (lower=faster and smoother)
  
  
  

  
//Begin switch conditionals
 //________________________________
 if (aState == HIGH && first == 0){
   off = 1;
   Serial.println("A on");
   Serial.println("Fading IN");
   //Fade in from min to max value
   
      while(1){ //using an inf loop to be more custom.
        aState = digitalRead(aPin);
        
        if (aState != HIGH){
          Serial.println("other switch triggered!");
          Serial.print("R=");
          off = 0;
          Serial.println(R);
          break;} //break loop to go to different mode
        
        //Protect the strand from higher then 255 values
        if(R>=255 || G>=255 || B>=255) { //DO NOT DELETE OR ALTER THIS LINE.
            break; //DO NOT DELETE OR ALTER THIS LINE.
            Serial.println("color max break!"); //DO NOT DELETE OR ALTER THIS LINE.
            delay(500);} //DO NOT DELETE OR ALTER THIS LINE.
      
        //break the inf loop if the color is higher then what its set at.
        if (R>Rset+1 && G>Gset+1 && B>Bset+1)  { 
          //ReSet the RGB to set values. 
          R=Rset;
          G=Gset;
          B=Bset;
          Serial.println("target color reached"); 
          delay(500);
          break; 
          } 
       //update the strip0
        for(int i=0; i<strip0.numPixels(); i++) {
          strip0.setPixelColor(i, strip0.Color(R, G, B));
          //delay(0);
          }
          strip0.show();
       //update the strip1
        for(int i=0; i<strip1.numPixels(); i++) {
          strip1.setPixelColor(i, strip1.Color(R, G, B));
          //delay(0);
          }
          strip1.show();
        //update the strip2
         for(int i=0; i<strip2.numPixels(); i++) {
          strip2.setPixelColor(i, strip2.Color(R, G, B));
          //delay(0);
          }
          strip2.show();
        //update the strip3
          for(int i=0; i<strip3.numPixels(); i++) {
          strip3.setPixelColor(i, strip3.Color(R, G, B));
          //delay(0);
          }
          strip3.show();
          
      //increase by the set amount
        R=R+finCount;
        G=G+finCount;
        B=B+finCount;
       delay(waitT);
         //Serial.println("going up");
         //Serial.println(R);
        }
    Serial.println("fade in completed!");
  
   if (R == Rset){
   colorON0(strip0.Color(Rset, Gset, Bset), 0); // White Note this is passed to the 'c' var in the function
   colorON1(strip1.Color(Rset, Gset, Bset), 0); // White
   colorON2(strip2.Color(Rset, Gset, Bset), 0); // White
   colorON3(strip3.Color(Rset, Gset, Bset), 0); // White
   first = 1;
   }
 }
 //________________________________  
 else if (bState == HIGH){
  delay(100);               // wait for a second
  Serial.println("B on");
  Serial.println("Pulsar");
  Serial.println("Fading IN");
  finCount=1;  //step in size (lower=slower and smoother)
  foutCount=1;  //step out size
   //Fade in from min to max value
   
      while(1){ //using an inf loop to be more custom.
        bState = digitalRead(bPin);
        
        if (bState != HIGH){
          Serial.println("other switch triggered!");
          Serial.print("R=");
          Serial.println(R);
          off = 0;
          break;} //break loop to go to different mode
        
        //Protect the strand from higher then 255 values
        if(R>=255 || G>=255 || B>=255) { //DO NOT DELETE OR ALTER THIS LINE.
            break; //DO NOT DELETE OR ALTER THIS LINE.
            Serial.println("color max break!"); //DO NOT DELETE OR ALTER THIS LINE.
            delay(500);} //DO NOT DELETE OR ALTER THIS LINE.
      
        //break the inf loop if the color is higher then what its set at.
        if (R>Rset+1 && G>Gset+1 && B>Bset+1)  { 
          //ReSet the RGB to set values. 
          R=Rset;
          G=Gset;
          B=Bset;
          Serial.println("target color reached"); 
          delay(500);
          break; 
          } 
       //update the strip0
        for(int i=0; i<strip0.numPixels(); i++) {
          strip0.setPixelColor(i, strip0.Color(R, G, B));
          //delay(0);
          }
          strip0.show();
       //update the strip1
        for(int i=0; i<strip1.numPixels(); i++) {
          strip1.setPixelColor(i, strip1.Color(R, G, B));
          //delay(0);
          }
          strip1.show();
        //update the strip2
         for(int i=0; i<strip2.numPixels(); i++) {
          strip2.setPixelColor(i, strip2.Color(R, G, B));
          //delay(0);
          }
          strip2.show();
        //update the strip3
          for(int i=0; i<strip3.numPixels(); i++) {
          strip3.setPixelColor(i, strip3.Color(R, G, B));
          //delay(0);
          }
          strip3.show();
          
      //increase by the set amount
        R=R+finCount;
        G=G+finCount;
        B=B+finCount;
       delay(waitT);
         //Serial.println("going up");
         //Serial.println(R);
        }
    Serial.println("fade in completed!");
    //fade out
    Serial.println("fading out pulse");
     while(1){ //using an inf loop to be more custom.
      bState = digitalRead(bPin);
        if (bState != HIGH){
          Serial.println("other switch triggered!");
          Serial.print("R=");
          Serial.println(R);
          off = 0;
          break;} //break loop to go to different mode
          
      //Protect the strand from higher then 255 values
      if(R>255 || G>255 || B>255) { break; Serial.println("Danger Max!");} //DO NOT DELETE OR ALTER THIS LINE.
      //break the inf loop if the color is off
      if (R<0 && G<0 && B<0)  { 
        //ReSet the RGB to 0 values. 
        R=0;
        G=0;
        B=0;
       // Serial.println("already at 0!");
        break; 
        } 
        
   //update the strip0
     for(int j=0; j<strip0.numPixels(); j++) {
      strip0.setPixelColor(j, strip0.Color(R, G, B));
      //delay(0);
       }
       strip0.show();
   //update the strip1
     for(int j=0; j<strip1.numPixels(); j++) {
      strip1.setPixelColor(j, strip1.Color(R, G, B));
      //delay(0);
       }
       strip1.show();
   //update the strip2
     for(int j=0; j<strip2.numPixels(); j++) {
      strip2.setPixelColor(j, strip2.Color(R, G, B));
      //delay(0);
       }
       strip2.show();
   //update the strip3
     for(int j=0; j<strip3.numPixels(); j++) {
      strip3.setPixelColor(j, strip3.Color(R, G, B));
      //delay(0);
       }
       strip3.show();
       
    //Decrease by the set amount
    R=R-foutCount;
    G=G-foutCount;
    B=B-foutCount;
    delay(waitT);
    //Serial.println("Fading");
  }
  }
//________________________________ 
 else if (cState == HIGH){
  //delay(500);               // wait for a second
  Serial.println("C on");
  Serial.println("party mode");
      //rainbow(20);
     //rainbowCycle(20);
    theaterChaseRainbow(50);
    //theaterChase(50);
  }
//________________________________
 else if ((dState == HIGH && first == 1) || (off == 0)){
   Serial.println("D on");
   
     while(1){ //using an inf loop to be more custom.
     
      //Protect the strand from higher then 255 values
      if(R>255 || G>255 || B>255) { break; Serial.println("Danger Max!");} //DO NOT DELETE OR ALTER THIS LINE.
      //break the inf loop if the color is off
      if (R<0 && G<0 && B<0)  { 
        //ReSet the RGB to 0 values. 
        R=0;
        G=0;
        B=0;
       // Serial.println("already at 0!");
        break; 
        } 
     Serial.println("Fade To Black");   
   //update the strip0
     for(int j=0; j<strip0.numPixels(); j++) {
      strip0.setPixelColor(j, strip0.Color(R, G, B));
      //delay(0);
       }
       strip0.show();
   //update the strip1
     for(int j=0; j<strip1.numPixels(); j++) {
      strip1.setPixelColor(j, strip1.Color(R, G, B));
      //delay(0);
       }
       strip1.show();
   //update the strip2
     for(int j=0; j<strip2.numPixels(); j++) {
      strip2.setPixelColor(j, strip2.Color(R, G, B));
      //delay(0);
       }
       strip2.show();
   //update the strip3
     for(int j=0; j<strip3.numPixels(); j++) {
      strip3.setPixelColor(j, strip3.Color(R, G, B));
      //delay(0);
       }
       strip3.show();
       
    //Decrease by the set amount
    R=R-foutCount;
    G=G-foutCount;
    B=B-foutCount;
    delay(waitT);
    //Serial.println("Fading");
  }
  
   Serial.println("Fade Completed");
   colorON0(strip0.Color(0, 0, 0), 0); // White Note this is passed to the 'c' var in the function
   colorON1(strip1.Color(0, 0, 0), 0); // White
   colorON2(strip2.Color(0, 0, 0), 0); // White
   colorON3(strip3.Color(0, 0, 0), 0); // White
   delay(100);               // wait for a second
   first = 0; //reset first counter
  }
  
  //Generic Call:  
  //function(strip(RGB),speed
 //colorON0(strip0.Color(255, 255, 255), 0); // White Note this is passed to the 'c' var in the function
 //colorON1(strip1.Color(255, 255, 255), 0); // White
 //colorON2(strip2.Color(255, 255, 255), 0); // White
 //colorON3(strip3.Color(255, 255, 255), 0); // White

}

//____________________________________________________________________________________________________________________________
//END MAIN LOOP^
//SUPPORT FUNCTIONS BELOW

// Fill Pixels All at once with a solid color called c 
void colorON0(uint32_t c, uint8_t wait) { //get c from call above!
  for(uint16_t i=0; i<strip0.numPixels(); i++) {
      strip0.setPixelColor(i, c);
      //delay(wait);
  }
  strip0.show(); //make sure 
}

void colorON1(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip1.numPixels(); i++) {
      strip1.setPixelColor(i, c);
      //delay(wait);
  }
   strip1.show();
}

void colorON2(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip2.numPixels(); i++) {
      strip2.setPixelColor(i, c);
      //delay(wait);
  }
  strip2.show();
}

void colorON3(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip3.numPixels(); i++) {
      strip3.setPixelColor(i, c);
      //delay(wait);
  }
   strip3.show();
}



void rainbow(uint8_t wait) {
  uint16_t i, j;
  for(j=0; j<256; j++) {
    for(i=0; i<strip0.numPixels(); i++) {
      strip0.setPixelColor(i, Wheel((i+j) & 255));
    }
    //strip0.show();
    //delay(wait);
  }
    for(j=0; j<256; j++) {
    for(i=0; i<strip1.numPixels(); i++) {
      strip1.setPixelColor(i, Wheel((i+j) & 255));
    }
    //strip1.show();
    //delay(wait);
  }
    for(j=0; j<256; j++) {
    for(i=0; i<strip2.numPixels(); i++) {
      strip2.setPixelColor(i, Wheel((i+j) & 255));
    }
    //strip2.show();
    //delay(wait);
  }
    for(j=0; j<256; j++) {
    for(i=0; i<strip3.numPixels(); i++) {
      strip3.setPixelColor(i, Wheel((i+j) & 255));
    }
    //strip3.show();
    //delay(wait);
  }
  strip0.show();
  strip1.show();
  strip2.show();
  strip3.show();
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip0.numPixels(); i++) {
      strip0.setPixelColor(i, Wheel(((i * 256 / strip0.numPixels()) + j) & 255));
    }
    strip0.show();
    delay(wait);
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip0.numPixels(); i=i+3) {
        strip0.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip0.show();
     
      delay(wait);
     
      for (int i=0; i < strip0.numPixels(); i=i+3) {
        strip0.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
        for (int i=0; i < strip0.numPixels(); i=i+3) {
          strip0.setPixelColor(i+q, Wheel( (i+j) % 255));
          strip1.setPixelColor(i+q, Wheel( (i+j) % 255));
          strip2.setPixelColor(i+q, Wheel( (i+j) % 255)); 
          strip3.setPixelColor(i+q, Wheel( (i+j) % 255));     //turn every third pixel on
        }
        strip0.show();
        strip1.show();
        strip2.show();
        strip3.show();
        delay(wait);
       
        for (int i=0; i < strip0.numPixels(); i=i+3) {
          strip0.setPixelColor(i+q, 0);
          strip1.setPixelColor(i+q, 0);
          strip2.setPixelColor(i+q, 0);
          strip3.setPixelColor(i+q, 0);        //turn every third pixel off
        }
    }
    cState = digitalRead(cPin);
        if (cState != HIGH){
          Serial.println("other switch triggered!");
          Serial.print("R=");
          Serial.println(R);
          off = 0;
          break;} //break loop to go to different mode
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return strip0.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return strip0.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip0.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
